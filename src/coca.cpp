/*
 * coca.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <cocaIPC/cocaIPC.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

namespace {
    typedef ProxyObjectBase<cocaIPC::Server, POA_cocaIPC::Server,
            POA_cocaIPC::Server_tie> CocaServerProxy;

    ProxyObjectManager::FactoryRegistrator<CocaServerProxy> __p1__;
}
