/*
 * pmg.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <pmgpriv/pmgpriv.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include "IPCGatewayProxy/ProxyObjectManager.hpp"

namespace {
    class PMGServerProxy: public ProxyObjectBase<pmgpriv::SERVER,
            POA_pmgpriv::SERVER, POA_pmgpriv::SERVER_tie> {
    public:
        using ProxyObjectBase<pmgpriv::SERVER, POA_pmgpriv::SERVER,
                POA_pmgpriv::SERVER_tie>::ProxyObjectBase;

        char * request_start(const pmgpriv::ProcessRequestInfo& i) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(i.client_ref));
            ProxyObjectManager::instance().updateInReference(ref);
            const_cast<pmgpriv::ProcessRequestInfo&>(i).client_ref =
                    pmgpriv::CLIENT::_narrow(ref);
            return m_target->request_start(i);
        }

        bool link_client(const pmgpriv::LinkRequestInfo& i) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(i.client_ref));
            ProxyObjectManager::instance().updateInReference(ref);
            const_cast<pmgpriv::LinkRequestInfo&>(i).client_ref =
                    pmgpriv::CLIENT::_narrow(ref);
            return m_target->link_client(i);
        }
    };

    typedef ProxyObjectBase<pmgpriv::CLIENT, POA_pmgpriv::CLIENT,
            POA_pmgpriv::CLIENT_tie> PMGClientProxy;

    ProxyObjectManager::FactoryRegistrator<PMGServerProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<PMGClientProxy> __p2__;
}
