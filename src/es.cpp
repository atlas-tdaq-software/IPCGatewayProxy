/*
 * es.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <es/es.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

namespace {
    typedef ProxyObjectBase<es::ExpertSystem, POA_es::ExpertSystem,
            POA_es::ExpertSystem_tie> ExpertSystemProxy;

    ProxyObjectManager::FactoryRegistrator<ExpertSystemProxy> __p1__;
}
