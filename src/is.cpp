/*
 * is.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <is/is.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include "IPCGatewayProxy/ProxyObjectManager.hpp"

namespace {
    class ISRepositoryProxy: public ProxyObjectBase<is::repository,
            POA_is::repository, POA_is::repository_tie> {
    public:
        using ProxyObjectBase<is::repository, POA_is::repository,
                POA_is::repository_tie>::ProxyObjectBase;

        void create_stream(const is::criteria & criteria, is::stream_ptr obj,
                CORBA::Long history_depth, is::sorted order) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->create_stream(criteria, is::stream::_narrow(ref),
                    history_depth, order);
        }

        void add_callback(const char * name, is::callback_ptr obj,
                CORBA::Long event_mask) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->add_callback(name, is::callback::_narrow(ref),
                    event_mask);
        }

        void remove_callback(const char * name, is::callback_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->remove_callback(name, is::callback::_narrow(ref));
        }

        void subscribe(const is::criteria & criteria, is::callback_ptr obj,
                CORBA::Long event_mask) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->subscribe(criteria, is::callback::_narrow(ref),
                    event_mask);
        }

        void unsubscribe(const is::criteria & criteria, is::callback_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->unsubscribe(criteria, is::callback::_narrow(ref));
        }
    };

    typedef ProxyObjectBase<is::stream, POA_is::stream, POA_is::stream_tie> ISStreamProxy;
    typedef ProxyObjectBase<is::callback, POA_is::callback, POA_is::callback_tie> ISCallbackProxy;
    typedef ProxyObjectBase<is::event_callback, POA_is::event_callback,
            POA_is::event_callback_tie> ISEventCallbackProxy;
    typedef ProxyObjectBase<is::info_callback, POA_is::info_callback,
            POA_is::info_callback_tie> ISInfoCallbackProxy;
    typedef ProxyObjectBase<is::provider, POA_is::provider, POA_is::provider_tie> ISProviderProxy;

    ProxyObjectManager::FactoryRegistrator<ISRepositoryProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<ISStreamProxy> __p2__;
    ProxyObjectManager::FactoryRegistrator<ISCallbackProxy> __p3__;
    ProxyObjectManager::FactoryRegistrator<ISEventCallbackProxy> __p4__;
    ProxyObjectManager::FactoryRegistrator<ISInfoCallbackProxy> __p5__;
    ProxyObjectManager::FactoryRegistrator<ISProviderProxy> __p6__;
}
