/*
 * rc.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <memory>

#include <ipc/core.h>

#include <rc/rc.hh>
#include <RunControl/Common/RunControlBasicCommand.h>
#include <RunControl/Common/RunControlCommands.h>

#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

using namespace daq::rc;

namespace {
    typedef ProxyObjectBase<rc::sender, POA_rc::sender,
            POA_rc::sender_tie> RCSenderProxy;

    class RCCommanderProxy : public ProxyObjectBase<rc::commander, POA_rc::commander,
        POA_rc::commander_tie> {

        using ProxyObjectBase<rc::commander, POA_rc::commander,
                POA_rc::commander_tie>::ProxyObjectBase;

    public:
        void executeCommand(const ::rc::SenderContext& sc, const char* cmdDescription) override {
            try {
                const std::shared_ptr<RunControlBasicCommand> cmd(
                        RunControlCommands::factory(cmdDescription));
                std::string sref = cmd->senderReference();
                if (not sref.empty()) {
                    CORBA::Object_var obj(IPCCore::stringToObject(sref));
                    ProxyObjectManager::instance().updateInReference(obj, rc::sender::_PD_repoId);
                    cmd->senderReference(IPCCore::objectToString(obj));
                    std::string cd = cmd->serialize();
                    m_target->executeCommand(sc, cd.c_str());
                    return;
                }
            } catch (ers::Issue & ex) {
                ers::warning(ex);
            }

            m_target->executeCommand(sc, cmdDescription);
        }

        void makeTransition(const ::rc::SenderContext& sc, const char* cmdDescription) override {
            try {
                const std::shared_ptr<TransitionCmd> cmd(
                        TransitionCmd::create(cmdDescription));
                std::string sref = cmd->senderReference();
                if (not sref.empty()) {
                    CORBA::Object_var obj(IPCCore::stringToObject(sref));
                    ProxyObjectManager::instance().updateInReference(obj, rc::sender::_PD_repoId);
                    cmd->senderReference(IPCCore::objectToString(obj));
                    std::string cd = cmd->serialize();
                    m_target->makeTransition(sc, cd.c_str());
                    return;
                }
            } catch (ers::Issue & ex) {
                ers::warning(ex);
            }

            m_target->makeTransition(sc, cmdDescription);
        }
    };

    ProxyObjectManager::FactoryRegistrator<RCCommanderProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<RCSenderProxy> __p2__;
}
