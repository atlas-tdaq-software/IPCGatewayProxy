/*
 * rdb.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <rdb/rdb.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include "IPCGatewayProxy/ProxyObjectManager.hpp"

namespace {
    class CursorProxy: public ProxyObjectBase<rdb::cursor, POA_rdb::cursor,
            POA_rdb::cursor_tie> {
    public:
        using ProxyObjectBase<rdb::cursor, POA_rdb::cursor, POA_rdb::cursor_tie>::ProxyObjectBase;

        CORBA::Long subscribe(const rdb::ProcessInfo& info,
                const rdb::RDBNameList& l1, bool lis,
                const rdb::RDBObjectList& l2, rdb::callback_ptr obj,
                CORBA::LongLong parameter) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            return m_target->subscribe(info, l1, lis, l2,
                    rdb::callback::_narrow(ref), parameter);
        }

        void get_complete_db(rdb::RDBCompleteDB_out db,
                const char * server_name, rdb::rdb_callback_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->get_complete_db(db, server_name,
                    rdb::rdb_callback::_narrow(ref));
        }
    };

    class WriterProxy: public ProxyObjectBase<rdb::writer, POA_rdb::writer,
            POA_rdb::writer_tie> {
    public:
        using ProxyObjectBase<rdb::writer, POA_rdb::writer, POA_rdb::writer_tie>::ProxyObjectBase;

        void subscribe(rdb::RDBSessionId session_id,
                const rdb::ProcessInfo& info, const rdb::RDBNameList& l1,
                bool lis, const rdb::RDBObjectList& l2, rdb::callback_ptr obj,
                CORBA::LongLong parameter) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->subscribe(session_id, info, l1, lis, l2,
                    rdb::callback::_narrow(ref), parameter);
        }
    };

    typedef ProxyObjectBase<rdb::ping, POA_rdb::ping, POA_rdb::ping_tie> PingProxy;
    typedef ProxyObjectBase<rdb::callback, POA_rdb::callback,
            POA_rdb::callback_tie> CallbackProxy;
    typedef ProxyObjectBase<rdb::rdb_callback, POA_rdb::rdb_callback,
            POA_rdb::rdb_callback_tie> RDBCallbackProxy;

    ProxyObjectManager::FactoryRegistrator<PingProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<CallbackProxy> __p2__;
    ProxyObjectManager::FactoryRegistrator<RDBCallbackProxy> __p3__;
    ProxyObjectManager::FactoryRegistrator<CursorProxy> __p4__;
    ProxyObjectManager::FactoryRegistrator<WriterProxy> __p5__;
}
