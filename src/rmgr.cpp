/*
 * rmgr.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <rmgr/rmgr.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

namespace {
    typedef ProxyObjectBase<rmgr::ResMgr, POA_rmgr::ResMgr, POA_rmgr::ResMgr_tie> ResMgrProxy;

    ProxyObjectManager::FactoryRegistrator<ResMgrProxy> __p1__;
}
