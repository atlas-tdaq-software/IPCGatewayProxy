/*
 * TriggerCommander.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: kolos
 */

#include <trgCommander/trgCommander.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

namespace {
    typedef ProxyObjectBase<TRIGGER::commander, POA_TRIGGER::commander,
            POA_TRIGGER::commander_tie> TriggerCommanderProxy;

    ProxyObjectManager::FactoryRegistrator<TriggerCommanderProxy> __p1__;
}
