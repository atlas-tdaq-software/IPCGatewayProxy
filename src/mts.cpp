/*
 * mts.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <mts/mts.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

namespace {
    typedef ProxyObjectBase<mts::receiver, POA_mts::receiver,
            POA_mts::receiver_tie> MTSReceiverProxy;
    typedef ProxyObjectBase<mts::worker, POA_mts::worker, POA_mts::worker_tie> MTSWorkerProxy;

    ProxyObjectManager::FactoryRegistrator<MTSReceiverProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<MTSWorkerProxy> __p2__;
}
