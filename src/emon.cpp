/*
 * rdb.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: kolos
 */

#include <emon/emon.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include "IPCGatewayProxy/ProxyObjectManager.hpp"

namespace {
    class SamplerProxy: public ProxyObjectBase<EventMonitoring::EventSampler,
            POA_EventMonitoring::EventSampler,
            POA_EventMonitoring::EventSampler_tie> {
    public:
        using ProxyObjectBase<EventMonitoring::EventSampler,
                POA_EventMonitoring::EventSampler,
                POA_EventMonitoring::EventSampler_tie>::ProxyObjectBase;

        void connect_monitor(const EventMonitoring::SelectionCriteria & c, const char *n,
                EventMonitoring::EventMonitor_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->connect_monitor(c, n,
                    EventMonitoring::EventMonitor::_narrow(ref));
        }

        void disconnect_monitor(const EventMonitoring::SelectionCriteria & c, const char *n,
                EventMonitoring::EventMonitor_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->disconnect_monitor(c, n,
                    EventMonitoring::EventMonitor::_narrow(ref));
        }
    };

    class ConductorProxy: public ProxyObjectBase<EventMonitoring::Conductor,
            POA_EventMonitoring::Conductor, POA_EventMonitoring::Conductor_tie> {
    public:
        using ProxyObjectBase<EventMonitoring::Conductor,
                POA_EventMonitoring::Conductor,
                POA_EventMonitoring::Conductor_tie>::ProxyObjectBase;

        void add_sampler(const EventMonitoring::SamplingAddress & a,
                EventMonitoring::EventSampler_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->add_sampler(a,
                    EventMonitoring::EventSampler::_narrow(ref));
        }

        void add_monitor(EventMonitoring::SamplingAddress & a,
                const EventMonitoring::SelectionCriteria & c, const char *n,
                EventMonitoring::EventMonitor_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->add_monitor(a, c, n,
                    EventMonitoring::EventMonitor::_narrow(ref));
        }

        void remove_monitor(const EventMonitoring::SamplingAddress & a,
                const EventMonitoring::SelectionCriteria & c, const char *n,
                EventMonitoring::EventMonitor_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            m_target->remove_monitor(a, c, n,
                    EventMonitoring::EventMonitor::_narrow(ref));
        }
    };

    ProxyObjectManager::FactoryRegistrator<SamplerProxy> __p2__;
    ProxyObjectManager::FactoryRegistrator<ConductorProxy> __p3__;
}
