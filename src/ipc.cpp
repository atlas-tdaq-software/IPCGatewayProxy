/*
 * ipc.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <ipc/ipc.hh>
#include <IPCGatewayProxy/ProxyObjectBase.hpp>
#include "IPCGatewayProxy/ProxyObjectManager.hpp"

namespace {
    using namespace CosNaming;

    template<class I, class POA_I, template<class > class POA_I_tie>
    class IPCProxyBase: public ProxyObjectBase<I, POA_I, POA_I_tie> {
    public:
        using ProxyObjectBase<I, POA_I, POA_I_tie>::ProxyObjectBase;

        void bind(const Name & n, CORBA::Object_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            this->m_target->bind(n, ref);
        }

        void rebind(const Name & n, CORBA::Object_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            this->m_target->rebind(n, ref);
        }

        void bind_context(const Name & n, NamingContext_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            NamingContext_var var = CosNaming::NamingContext::_narrow(ref);
            this->m_target->bind_context(n, var);
        }

        void rebind_context(const Name & n, NamingContext_ptr obj) override {
            CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
            ProxyObjectManager::instance().updateInReference(ref);
            NamingContext_var var = CosNaming::NamingContext::_narrow(ref);
            this->m_target->rebind_context(n, var);
        }

        CORBA::Object_ptr resolve(const Name & n) override {
            CORBA::Object_var ref = this->m_target->resolve(n);
            ProxyObjectManager::instance().updateOutReference(ref);
            return CORBA::Object::_duplicate(ref);
        }

        void associate(const Name & n, const ipc::URI & uri) override {
            std::string ior((const char *) uri.get_buffer());
            CORBA::Object_var ref = IPCCore::stringToObject(ior);
            ProxyObjectManager::instance().updateInReference(ref);
            ior = IPCCore::objectToString(ref);
            ipc::URI u(ior.size() + 1, ior.size() + 1,
                    (CORBA::Octet*) ior.c_str(), false);
            ERS_DEBUG(2,
                    "IOR was replaced with '"
                            << IPCCore::objectToString(ref, IPCCore::Corbaloc)
                            << "'");
            this->m_target->associate(n, u);
        }

        void obtain(const Name & n, ipc::URI_out uri) override {
            this->m_target->obtain(n, uri);
            std::string ior((const char *) uri->get_buffer());
            CORBA::Object_var ref = IPCCore::stringToObject(ior);
            ProxyObjectManager::instance().updateOutReference(ref);
            ior = IPCCore::objectToString(ref);
            *uri.ptr() = ipc::URI(ior.size() + 1, ior.size() + 1,
                    (CORBA::Octet*) ior.c_str(), false);
            ERS_DEBUG(2,
                    "IOR was replaced with '"
                            << IPCCore::objectToString(ref, IPCCore::Corbaloc)
                            << "'");
        }
    };

    typedef IPCProxyBase<ipc::NamingContextOpt, POA_ipc::NamingContextOpt,
            POA_ipc::NamingContextOpt_tie> NamingContextOptProxy;
    typedef IPCProxyBase<ipc::partition, POA_ipc::partition,
            POA_ipc::partition_tie> PartitionProxy;

    ProxyObjectManager::FactoryRegistrator<NamingContextOptProxy> __p1__;
    ProxyObjectManager::FactoryRegistrator<PartitionProxy> __p2__;
}
