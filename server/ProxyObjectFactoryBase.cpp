/*
 * ProxyObjectFactoryBase.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */
#include <iomanip>
#include <sstream>

#include <IPCGatewayProxy/ProxyObjectFactoryBase.hpp>

#include <ipc/util.h>

std::string ProxyObjectFactoryBase::getObjectIdFromIOR(const std::string & ior) {
    const char * ptr = ior.c_str() + 4;
    cdrMemoryStream buf(strlen(ptr) / 2, 0);
    char hex[3] = { 0, 0, 0 };
    while (*ptr) {
        hex[0] = *ptr++;
        hex[1] = *ptr++;
        unsigned int v;
        sscanf(hex, "%x", &v);
        buf.marshalOctet((CORBA::Octet) v);
    }
    buf.rewindInputPtr();
    CORBA::Boolean byteOrder = buf.unmarshalBoolean();
    buf.setByteSwapFlag(byteOrder);

    const char * tid = IOP::IOR::unmarshaltype_id(buf);
    delete[] tid;
    IOP::TaggedProfileList profiles;
    profiles <<= buf;

    for (size_t i = 0; i < profiles.length(); i++) {
        if (profiles[i].tag == IOP::TAG_INTERNET_IOP) {
            _CORBA_Unbounded_Sequence_Octet object_key;
            IIOP::unmarshalObjectKey(profiles[i], object_key);
            size_t start = object_key.length();
            while (--start > 0){
            	if (object_key[start] == 0){
                    break;
                }
            }
            std::ostringstream out;
            for (size_t i = start+1; i < object_key.length(); i++) {
                if (ipc::util::isValidChar(object_key[i])) {
                    out << object_key[i];
                }
                else {
                    out << "%" << std::hex << std::setw(2) << std::setfill('0')
                            << (int) object_key[i];
                }
            }
            return out.str();
        }
    }
    return "";
}
