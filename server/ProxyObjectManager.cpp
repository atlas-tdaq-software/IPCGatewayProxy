/*
 * ProxyObjactManager.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/util.h>

#include <IPCGatewayProxy/ProxyObjectFactoryBase.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

ProxyObjectManager& ProxyObjectManager::instance() {
    static ProxyObjectManager manager;
    return manager;
}

void ProxyObjectManager::clearCache() {
    std::scoped_lock<std::mutex> lock(m_mutex);
    ERS_DEBUG(1, "Clearing proxy objects cache");
    std::for_each(m_factories.begin(), m_factories.end(),
            [](auto & pair) {pair.second->clearCache();});
}

void ProxyObjectManager::registerFactoryCreator(FactoryCreator creator) {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_factory_creators.push_back(creator);
    ERS_DEBUG(1, "Object Factory creator has been registered");
}

void ProxyObjectManager::createFactories(const std::string & backupDirectory) {
    std::unique_lock<std::mutex> lock(m_mutex);
    for (auto c : m_factory_creators) {
        std::shared_ptr<ProxyObjectFactoryBase> factory = c(backupDirectory);
        m_factories[factory->getTypeId()] = factory;
        ERS_DEBUG(1, "Object Factory for the '"
                << factory->getTypeId() << "' interface has been registered");
    }
}

void ProxyObjectManager::runGarbageCollector() {
    Factories factories;
    {
        std::scoped_lock<std::mutex> lock(m_mutex);
        factories = m_factories;
    }
    ERS_DEBUG(1, "Starting the garbage collection");
    std::for_each(factories.begin(), factories.end(),
            [](auto & pair) {pair.second->runGarbageCollector();});
    ERS_DEBUG(1, "Garbage collection has finished");
}

void ProxyObjectManager::replaceByProxy(CORBA::Object_var & obj,
        const std::string & type) {
    std::unique_lock<std::mutex> lock(m_mutex);
    Factories::iterator it = m_factories.find(type);
    if (it != m_factories.end()) {
        ERS_DEBUG(1, "Object Factory for the '" << type
                        << "' interface has been found");

        Factory f = it->second;
        lock.unlock();
        obj = f->createProxyObject(obj);
        return;
    }
    ERS_DEBUG(1, "Object Factory for the '" << type
                    << "' interface has not been found");
}

void ProxyObjectManager::replaceByProxy(CORBA::Object_var & obj) {
    if (CORBA::is_nil(obj)) {
        ERS_DEBUG(1, "nil object reference is ignored");
        return ;
    }

    std::string key = obj->_PR_getobj()->_mostDerivedRepoId();
    replaceByProxy(obj, key);
}

CORBA::Object_var ProxyObjectManager::createInitialProxy() {
    static const std::string key("IDL:ipc/partition:1.0");
    CORBA::Object_var obj = ipc::partition::_duplicate(IPCPartition().getImplementation());
    replaceByProxy(obj, key);
    return CORBA::Object::_duplicate(obj);
}

CORBA::Object_ptr ProxyObjectManager::getRemoteObject(CORBA::Object_ptr obj) {
    if (CORBA::is_nil(obj)) {
        ERS_DEBUG(1, "nil object reference is ignored");
        return CORBA::Object::_nil();
    }

    std::string key = obj->_PR_getobj()->_mostDerivedRepoId();

    std::unique_lock<std::mutex> lock(m_mutex);
    Factories::iterator it = m_factories.find(key);
    if (it != m_factories.end()) {
        ERS_DEBUG(1, "Object Factory for the '" << key
                        << "' interface has been found");

        Factory f = it->second;
        lock.unlock();
        return f->getRemoteObject(obj);
    }

    ERS_DEBUG(1, "Object Factory for the '" << key
                    << "' interface has not been found");
    return CORBA::Object::_nil();
}

void ProxyObjectManager::updateInReference(CORBA::Object_var & obj, const std::string & type) {
    try {
        std::string ref = IPCCore::objectToString(obj, IPCCore::Corbaloc);
        obj = m_proxy_twin->getProxyForObjectOfType(obj, type.c_str());
        ERS_LOG("Object reference '" << ref
            << "' has been replaced with the reference to the proxy object '"
            << IPCCore::objectToString(obj, IPCCore::Corbaloc));
    } catch (CORBA::SystemException & ex) {
        ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
}

void ProxyObjectManager::updateInReference(CORBA::Object_var & obj) {
    try {
        std::string ref = IPCCore::objectToString(obj, IPCCore::Corbaloc);
        obj = m_proxy_twin->getProxyForObject(obj);
        ERS_LOG("Object reference '" << ref
            << "' has been replaced with the reference to the proxy object '"
            << IPCCore::objectToString(obj, IPCCore::Corbaloc));
    } catch (CORBA::SystemException & ex) {
        ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
}


void ProxyObjectManager::updateOutReference(CORBA::Object_var & obj) {
    try {
        std::string ref = IPCCore::objectToString(obj, IPCCore::Corbaloc);
        CORBA::Object_var target = m_proxy_twin->getObjectForProxy(obj);

        if (!CORBA::is_nil(target)) {
            obj = CORBA::Object::_duplicate(target);
            ERS_LOG("Proxy object reference '" << ref
                << "' has been replaced with the reference to the real object '"
                << IPCCore::objectToString(target, IPCCore::Corbaloc));
        } else {
            replaceByProxy(obj);
            ERS_LOG("Original object reference '" << ref
                << "' has been replaced with the reference to the proxy '"
                << IPCCore::objectToString(obj, IPCCore::Corbaloc));
        }
    } catch (CORBA::SystemException & ex) {
        ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
}

void ProxyObjectManager::init(const std::string & scope,
        const std::string & remote_ref, const std::string & backup_dir) {
    try {
        CORBA::Object_var object = IPCCore::stringToObject(remote_ref);
        m_proxy_twin = IPCGateway::Proxy::_unchecked_narrow(object);

        if (CORBA::is_nil(m_proxy_twin)) {
            ERS_LOG("Can not connect to remote proxy: " << remote_ref);
        }
    }
    catch(daq::ipc::Exception & ex) {
        ERS_LOG("Can not connect to remote proxy: "
                << remote_ref << ", exception: " << ex);
    }
    catch(CORBA::SystemException & ex) {
        ERS_LOG("Can not connect to remote proxy: "
                << remote_ref << ", exception: " << ex._name());
    }

    // Create proxy object factories
    createFactories(backup_dir + "/" + scope);
}
