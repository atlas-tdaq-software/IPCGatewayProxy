/*
 * main.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#include <signal.h>
#include <sys/stat.h>

#include <fstream>
#include <thread>

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <ers/ers.h>
#include <ipc/util.h>

#include <IPCGatewayProxy/ProxyObjectManager.hpp>
#include <IPCGatewayProxy/ProxyImpl.hpp>

bool signalReceived = false;

void signal_handler(int) {
    signalReceived = true;
}

using namespace boost::program_options;

int main(int argc, char** argv) {
    {
        // Do not use configuration file as it may contain
        // 'endpoint' options which must not be used for the proxy
        ::setenv("OMNIORB_CONFIG", "this_file_must_not_exist", 1);
    }

    std::list<std::pair<std::string, std::string>> options;
    try {
        options = IPCCore::extractOptions(argc, argv);
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        return 1;
    }

    options_description description("Program Usage");
    description.add_options()
        ("help,h",
                "produce help message")
        ("check-initial,c",
                "check existence of the initial partition")
        ("private-ip,i", value<std::string>()->required(),
            	"private network interface")
        ("public-ip,I", value<std::string>()->required(),
                "public network interface")
        ("private-port,p", value<uint32_t>()->default_value(12345),
                "private port number")
        ("public-port,P", value<uint32_t>()->default_value(12346),
                "public port number")
        ("backup-dir,b", value<std::string>()->default_value("/tmp/ipc_gateway_proxy_backup"),
                "the backup files directory")
        ("public-ep,E", value<std::string>()->required(),
                "endpoint to be published by the public proxy")
        ("private-ep,e", value<std::string>()->required(),
                "endpoint to be published by the private proxy")
        ("gb-period,g", value<uint32_t>()->default_value(600),
		"period in seconds between garbage collector activations" );

    variables_map arguments;
    try {
        store(parse_command_line(argc, argv, description), arguments);

        if (arguments.count("help")) {
            std::cout << "IPC/CORBA gateway proxy application" << std::endl;
            description.print(std::cout);
            return 0;
        }

        notify(arguments);
    } catch (error & ex) {
        std::cerr << "ERROR: " << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    uint32_t private_port = arguments["private-port"].as<uint32_t>();
    uint32_t public_port = arguments["public-port"].as<uint32_t>();
    std::string private_ip = arguments["private-ip"].as<std::string>();
    std::string public_ip = arguments["public-ip"].as<std::string>();
    std::string backup_dir = arguments["backup-dir"].as<std::string>();
    std::string public_ep = arguments["public-ep"].as<std::string>();
    std::string private_ep = arguments["private-ep"].as<std::string>();

    ProxyImpl * proxy;
    
    int pid = fork();
    if (pid == -1) {
        ERS_LOG("Fork failed");
        return 1;
    }

    if (pid == 0) {
        // child process, create public proxy

        options.push_front(std::make_pair(std::string("endPoint"),
                "giop:tcp:" + public_ip + ":" + boost::lexical_cast<std::string>(public_port)));

        if (not public_ep.empty()) {
            options.push_front(std::make_pair(std::string("endPointPublish"),
                    public_ep));
        }

        std::string private_ref = ipc::util::constructReference(
		"private", 
                ipc::util::getTypeName<IPCGateway::Proxy>(), 
                std::vector<std::pair<std::string,int>>(
                	1, std::make_pair(private_ip, private_port)));        
        try {
            IPCCore::init(options);
            proxy = new ProxyImpl("public");
	    CORBA::Object_var tmp = proxy->_this();
            ProxyObjectManager::instance().init("public", private_ref, backup_dir);
        }
        catch (daq::ipc::Exception & ex) {
            ers::fatal(ex);
            return 2;
        }

        ERS_LOG("Public IPC gateway proxy has been started");
    }
    else {
        // parent process, create private proxy

        options.push_front(std::make_pair(std::string("endPoint"),
        	"giop:tcp:" + private_ip + ":" + boost::lexical_cast<std::string>(private_port)));

        if (not private_ep.empty()) {
            options.push_front(std::make_pair(std::string("endPointPublish"),
                    private_ep));
        }

        std::string public_ref = ipc::util::constructReference(
		"public",
                ipc::util::getTypeName<IPCGateway::Proxy>(), 
                std::vector<std::pair<std::string,int>>(
                	1, std::make_pair(public_ip, public_port)));        
        try {
            IPCCore::init(options);
            proxy = new ProxyImpl("private");
	    CORBA::Object_var tmp = proxy->_this();
            ProxyObjectManager::instance().init("private", public_ref, backup_dir);
        }
        catch (daq::ipc::Exception & ex) {
            ers::fatal(ex);
            kill(pid, SIGTERM);
            return 2;
        }

        CORBA::Object_var initial_proxy_ref;
        try {
            // Create proxy for the initial partition
            IPCPartition partition;
            if (arguments.count("check-initial") && !partition.isValid()) {
                ers::fatal(daq::ipc::InvalidPartition(ERS_HERE, ipc::partition::default_name));
                kill(pid, SIGTERM);
                return 3;
            }
            initial_proxy_ref = ProxyObjectManager::instance().createInitialProxy();
        }
        catch(daq::ipc::Exception & ex) {
            ers::fatal(ex);
            kill(pid, SIGTERM);
            return 3;
        }

        ERS_LOG("Private IPC gateway proxy has been started");
        std::string IOR = IPCCore::objectToString(initial_proxy_ref, IPCCore::Corbaloc);
        std::cout << "Proxy Object reference = " << IOR << std::endl;
    }
    
    //////////////////////////////////////////
    // Set hooks for signals
    //////////////////////////////////////////
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
    
    const uint32_t timeout = arguments["gb-period"].as<uint32_t>() * 1000;
    uint32_t timer = 0;
    while (!signalReceived) {
	usleep(1000); // sleep for 1ms
	if (++timer == timeout) {
            ProxyObjectManager::instance().runGarbageCollector();
            timer = 0;
	}
    }

    // This function must be called to destroy all proxy objects before
    // the  IPCCore singleton is shutdown
    ProxyObjectManager::instance().clearCache();
    
    proxy->_destroy();
    
    return 0;
}


