/*
 * PersistentStorage.cpp
 *
 *  Created on: Nov 15, 2018
 *      Author: kolos
 */

#include <IPCGatewayProxy/PersistentStorage.hpp>

PersistentStorage::PersistentStorage(const std::string & directory) :
        m_backup_file(directory) {

    try {
        boost::filesystem::path path(m_backup_file.parent_path());
        if (!exists(path)) {
            boost::filesystem::create_directories(path);
        }

        // Open(Create) the backup file
        m_stream.open(m_backup_file.string().c_str(),
                std::ios_base::in | std::ios_base::out);
    }
    catch (std::exception & ex) {
        throw CannotCreatePersistentStorage(ERS_HERE, m_backup_file.string(), ex);
    }

    ERS_DEBUG(1, "The '" << m_backup_file.string()
            << "' persistent storage has been initialized");
}

void PersistentStorage::save(const std::string & str) {
    std::unique_lock<std::mutex> lock(m_mutex);
    write(str);
}

void PersistentStorage::clear() {
    std::unique_lock<std::mutex> lock(m_mutex);
    reset();
}

void PersistentStorage::reset() {
    m_stream.close();
    m_stream.open(m_backup_file.string().c_str(),
                    std::ios_base::trunc | std::ios_base::out);
    m_stream.close();
    m_stream.open(m_backup_file.string().c_str(),
            std::ios_base::in | std::ios_base::out);
}

void PersistentStorage::updateAll(const std::vector<std::string> & strs) {
    std::unique_lock<std::mutex> lock(m_mutex);
    reset();
    for (const std::string & s : strs) {
        write(s);
    }
}

std::vector<std::string> PersistentStorage::readAll() {
    std::vector<std::string> result;

    std::unique_lock<std::mutex> lock(m_mutex);
    m_stream.seekg(0);
    for (std::string line; std::getline(m_stream, line); ) {
        result.push_back(line);
    }
    m_stream.seekg(0);
    return result;
}

void PersistentStorage::write(const std::string & str) {
    m_stream << str << std::endl;
}
