/*
 * ProxyImpl.cpp
 *
 *  Created on: Mar 14, 2019
 *      Author: kolos
 */

#include <IPCGatewayProxy/ProxyImpl.hpp>
#include <IPCGatewayProxy/ProxyObjectManager.hpp>

CORBA::Object_ptr ProxyImpl::getProxyForObject(CORBA::Object_ptr obj) {
    CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
    ProxyObjectManager::instance().replaceByProxy(ref);
    ERS_LOG("Object reference '" << IPCCore::objectToString(obj, IPCCore::Corbaloc)
        << "' has been replaced with the reference to the proxy object '"
        << IPCCore::objectToString(ref, IPCCore::Corbaloc));
    return CORBA::Object::_duplicate(ref);
}

CORBA::Object_ptr ProxyImpl::getProxyForObjectOfType(CORBA::Object_ptr obj, const char * type) {
    CORBA::Object_var ref(CORBA::Object::_duplicate(obj));
    ProxyObjectManager::instance().replaceByProxy(ref, type);
    ERS_LOG("Object reference '" << IPCCore::objectToString(obj, IPCCore::Corbaloc)
        << "' has been replaced with the reference to the proxy object '"
        << IPCCore::objectToString(ref, IPCCore::Corbaloc));
    return CORBA::Object::_duplicate(ref);
}

CORBA::Object_ptr ProxyImpl::getObjectForProxy(CORBA::Object_ptr obj) {
    return CORBA::Object::_duplicate(
            ProxyObjectManager::instance().getRemoteObject(obj));
}

