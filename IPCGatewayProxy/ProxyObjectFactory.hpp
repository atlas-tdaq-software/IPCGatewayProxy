/*
 * ProxyObjectFactory.hpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#ifndef IPCGATEWAYPROXY_PROXYOBJECTFACTORY_HPP_
#define IPCGATEWAYPROXY_PROXYOBJECTFACTORY_HPP_

#include <mutex>
#include <string>
#include <unordered_map>

#include <ipc/object.h>

#include <IPCGatewayProxy/PersistentStorage.hpp>
#include <IPCGatewayProxy/ProxyObjectFactoryBase.hpp>

/*
 * This is the base class for implementing a proxy object factory for an IDL interface.
 * Such implementation has to use the ProxyObjectBase class as template type parameter.
 * The ProxyObjectBase class has to use classes generated from the given IDL interface
 * as its template parameters.
 */
template<class PType>
class ProxyObjectFactory: public ProxyObjectFactoryBase {
public:
    ProxyObjectFactory(const std::string & backupDirectory) :
            m_type(PType::getTypeId()),
            m_storage(backupDirectory + "/" + m_type)
    {
        ERS_DEBUG(1, "The '" << m_type << "' object factory is created");
        std::vector<std::string> iors = m_storage.readAll();
        m_storage.reset();

        for (std::string & ior : iors) {
            ERS_DEBUG(3, "Restoring '" << ior << "' object");
            try {
                CORBA::Object_var object = IPCCore::stringToObject(ior);
                if (!CORBA::is_nil(object) && !object->_non_existent()) {
                    createProxyObject(object);
                } else {
                    ERS_DEBUG(3, "Proxy for the '" << ior
                            << "' object is not restored as it does not exist");
                }
            }
            catch (CORBA::SystemException & ex) {
                ERS_DEBUG(3,
                        "Restoring '" << ior << "' object failed: "
                        << daq::ipc::CorbaSystemException(ERS_HERE, &ex));
            }
            ERS_DEBUG(3,
                    "The '" << ior << "' object has been successfully restored");
        }
    }

    ~ProxyObjectFactory() {
        ERS_DEBUG(1, "The '" << m_type << "' object factory is destroyed");
    }

    /*
     * Destroys all proxy objects created by this factory.
     */
    void clearCache() override {
        ERS_DEBUG(1, "Clearing '" << getTypeId() << "' objects cache");
        std::scoped_lock<std::mutex> lock(m_mutex);
        m_cache.clear();
    }

    /*
     * Runs the garbage collector that destroys proxy objects with dead targets.
     */
    void runGarbageCollector() override {
        Cache cache;
        {
            std::scoped_lock<std::mutex> lock(m_mutex);
            cache = m_cache;
        }

        typename Cache::iterator it = cache.begin();
        ERS_DEBUG(1,
                "Starting garbage collection for '" << getTypeId()
                        << "' objects");
        uint32_t removedCnt = 0;
        for (; it != cache.end(); ++it) {
            if (!it->second->isTargetExist()) {
                ERS_DEBUG(1,
                        "Target for the '" << it->second->unique_id()
                                << "' object does not exist, removing it");
                std::scoped_lock < std::mutex > lock(m_mutex);
                typename Cache::iterator err = m_cache.find(it->first);
                if (err != m_cache.end()) {
                    m_cache.erase(err);
                    ++removedCnt;
                }
            }
        }
        ERS_DEBUG(1,
                "Garbage collection for '" << getTypeId()
                        << "' objects is finished, " << removedCnt
                        << " references have been removed");

        if (removedCnt) {
            // If some objects were removed from the cache the persistent storage has to be updated
            std::unique_lock<std::mutex> lock(m_mutex);
            std::vector<std::string> iors;
            std::for_each(m_cache.begin(), m_cache.end(), [&iors](const auto & pair)
                            { iors.push_back(pair.second->getTargetIOR()); } );
            m_storage.updateAll(iors);
        }
    }

    /*
     * Returns type id of the objects created by this factory.
     */
    const std::string & getTypeId() override {
        return m_type;
    }

    /*
     * Extracts the object id from the \p ref object reference and checks the
     * local cache for a proxy object with the same id. If no proxy object is
     * found the function will create a new proxy object and set the original
     * reference as its target object for requests forwarding.
     * If a proxy object with the same id already exists, the function will
     * replace its target reference with \p ref if the \p ref is not a reference
     * to the proxy object itself.
     *
     * \return The local proxy object reference.
     */
    CORBA::Object_var createProxyObject(CORBA::Object_ptr obj) override {
        std::string ior = IPCCore::objectToString(obj);
        std::string oid = getObjectIdFromIOR(ior);
	ERS_DEBUG(2, "Input object reference '" << ior << "' object id '" << oid << "'");
        
        std::scoped_lock<std::mutex> lock(m_mutex);
        typename Cache::iterator it = m_cache.find(oid);

        ObjectHolder r;
        if (it == m_cache.end()) {
	    ERS_DEBUG(2, "Creating new proxy for '" << oid << "' object");
            r = ObjectHolder(oid, obj);
            m_cache[oid] = r;
            m_storage.save(ior);
            ERS_DEBUG(1,
                    "New proxy object with '" << oid
                            << "' id has been created");
        }
        else {
            r = it->second;
            if (r->updateRemoteReference(obj)) {
                ERS_DEBUG(1, "Remote reference for the proxy object with id '" << oid
                                << "' has been updated");
                m_storage.save(ior);
            }
        }
        return r->getSelfReference();
    }

    CORBA::Object_ptr getRemoteObject(CORBA::Object_ptr obj) override {
        std::string oid = getObjectIdFromIOR(IPCCore::objectToString(obj));

        std::scoped_lock<std::mutex> lock(m_mutex);
        typename Cache::iterator it = m_cache.find(oid);

        ObjectHolder r;
        if (it != m_cache.end()) {
            r = it->second;
            ERS_DEBUG(1,
                    "Remote reference for the proxy object with id '" << oid
                            << "' has been found");
            return r->getTargetReference();
        }
        return CORBA::Object::_nil();
    }

private:

    /*
     * Utility structure which is used for properly destroying IPC objects.
     */
    class ObjectHolder: public std::shared_ptr<PType> {
    public:
        ObjectHolder() = default;

        template <class ...Args>
        ObjectHolder(Args ...args) :
            std::shared_ptr<PType>(new PType(args...),
                    [](PType * ptr) {if (ptr) {ptr->_destroy();}})
        {
        }
    };

private:
    typedef std::unordered_map<std::string, ObjectHolder> Cache;

    const std::string m_type;
    std::mutex m_mutex;
    Cache m_cache;
    PersistentStorage m_storage;
};

#endif /* IPCGATEWAYPROXY_PROXYOBJECTFACTORY_HPP_ */
