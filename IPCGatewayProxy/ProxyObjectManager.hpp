/*
 * ProxyObjectManager.hpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#ifndef IPCGATEWAYPROXY_PROXYOBJECTMANAGER_HPP_
#define IPCGATEWAYPROXY_PROXYOBJECTMANAGER_HPP_

#include <mutex>
#include <unordered_map>

#include <IPCGatewayProxy/ProxyObjectFactory.hpp>
#include <IPCGateway/IPCGateway.hh>

class ProxyObjectFactoryBase;

/*
 * This is the public interface for controlling proxy object factories.
 */
class ProxyObjectManager {
    ProxyObjectManager() = default;

public:
    /*
     * A helper class for creating and registering a proxy object factory.
     */
    template<class P>
    struct FactoryRegistrator {
        FactoryRegistrator() {
            instance().registerFactoryCreator(
                [](const std::string & s){
                        return std::make_shared<ProxyObjectFactory<P>>(s);
                });
        }
    };

    static ProxyObjectManager& instance();

    /*
     * Removes all proxy objects created so far.
     */
    void clearCache();

    /*
     * Returns the target object for the given proxy. If the proxy object
     * does not exist, returns CORBA::Object::_nil()
     */
    CORBA::Object_ptr getRemoteObject(CORBA::Object_ptr obj);

    /*
     * Run the garbage collector that removes proxy objects for dead targets.
     */
    void runGarbageCollector();

    /*
     * This is the main public function of the ProxyObjectManager class, which
     * should be used in a user custom proxy implementation for a private IDL interface.
     * This function has to be called for every 'in' and 'inout' CORBA::Object parameter
     * of a remote method call before passing them to the remote target of the current
     * proxy object.
     */
    void updateInReference(CORBA::Object_var & ref);

    void updateInReference(CORBA::Object_var & ref, const std::string & type);

    /*
     * This is the main public function of the ProxyObjectManager class, which
     * should be used in a user custom proxy implementation for a private IDL interface.
     * This function has to be called for every 'out' and 'inout' CORBA::Object parameter
     * of a remote method call after getting them from the remote target of the current
     * proxy object.
     */
    void updateOutReference(CORBA::Object_var & ref);

    /*
     * Initializes the ProxyObjectManager singleton with the given parameters.
     * This function must be called exactly once before using any other method
     * of this singleton object.
     */
    void init(const std::string & scope, const std::string & remote_ref,
                const std::string & backup_dir);

    /*
     * Extract the type id from the given object reference and and tries to find the
     * corresponding proxy object factory. If no factory is registered for this
     * type the function terminates leaving the \p ref parameter untouched. Otherwise
     * it replaces the \p ref object reference with the one returned by the factory
     * \ref createProxyObject(CORBA::Object_ptr) function.
     */
    void replaceByProxy(CORBA::Object_var & ref);
    
    void replaceByProxy(CORBA::Object_var & obj, const std::string & type);

    CORBA::Object_var createInitialProxy();

    /*
     * Instantiates all registered factories.
     */
    void createFactories(const std::string & backupDirectory);

private:
    typedef std::shared_ptr<ProxyObjectFactoryBase> Factory;
    typedef std::function<Factory(const std::string &)> FactoryCreator;

    /*
     * Registers a new proxy object factory creator.
     */
    void registerFactoryCreator(FactoryCreator creator);

private:
    typedef std::vector<FactoryCreator> FactoryCreators;
    typedef std::unordered_map<std::string, Factory> Factories;

    std::mutex m_mutex;
    FactoryCreators m_factory_creators;
    Factories m_factories;
    IPCGateway::Proxy_var m_proxy_twin;
};

#endif /* IPCGATEWAYPROXY_PROXYOBJECTMANAGER_HPP_ */
