/*
 * ProxyImpl.hpp
 *
 *  Created on: Mar 14, 2019
 *      Author: kolos
 */

#ifndef IPCGATEWAYPROXY_PROXYIMPL_HPP_
#define IPCGATEWAYPROXY_PROXYIMPL_HPP_

#include <IPCGateway/IPCGateway.hh>
#include <ipc/object.h>

class ProxyImpl : public IPCObject<POA_IPCGateway::Proxy,ipc::multi_thread,ipc::persistent>
{    
  public:
    ProxyImpl(const std::string & id) : m_id(id) {
    }
    
    CORBA::Object_ptr getProxyForObject(CORBA::Object_ptr object);
    
    CORBA::Object_ptr getProxyForObjectOfType(CORBA::Object_ptr object, const char * type);

    CORBA::Object_ptr getObjectForProxy(CORBA::Object_ptr object);
    
    std::string unique_id() const {
    	return m_id;
    }
    
  private:
    const std::string m_id;
};

#endif /* IPCGATEWAYPROXY_PROXYIMPL_HPP_ */
