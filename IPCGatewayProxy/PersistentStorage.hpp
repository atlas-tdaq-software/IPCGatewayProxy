/*
 * PersistentStorage.hpp
 *
 *  Created on: Nov 15, 2018
 *      Author: kolos
 */

#ifndef IPCGATEWAYPROXY_PERSISTENTSTORAGE_HPP_
#define IPCGATEWAYPROXY_PERSISTENTSTORAGE_HPP_

#include <fstream>
#include <mutex>
#include <string>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <ers/ers.h>

ERS_DECLARE_ISSUE(, CannotCreatePersistentStorage,
        "Can not create '" << name << "' persistent storage",
        ((std::string)name ))

/*
 * This class implements persistent storage for a flat collection of strings.
 * It is used to store IORs of the external CORBA objects.
 */
class PersistentStorage {
public:
    PersistentStorage(const std::string & directory);

    /*
     * Writes the given string to the end of this storage
     */
    void save(const std::string & str);

    /*
     * Clears the current content of this storage and writes the given strings to it
     */
    void updateAll(const std::vector<std::string> & strs);

    /*
     * Clears the storage
     */
    void clear();

    /*
     * Returns all strings available in this storage
     */
    std::vector<std::string> readAll();

    /*
     * Removes all information from this storage
     */
    void reset();

private:
    void write(const std::string & str);

private:
    mutable std::mutex m_mutex;
    boost::filesystem::path m_backup_file;
    std::fstream m_stream;
};

#endif /* IPCGATEWAYPROXY_PERSISTENTSTORAGE_HPP_ */
