/*
 * ProxyObjectFactoryBase.hpp
 *
 *  Created on: Oct 25, 2018
 *      Author: kolos
 */

#ifndef IPCGATEWAYPROXY_PROXYOBJECTFACTORYBASE_HPP_
#define IPCGATEWAYPROXY_PROXYOBJECTFACTORYBASE_HPP_

#include <string>

#include <omniORB4/CORBA.h>

/*
 * This class defines the common interface for a proxy object factory implementation.
 * This interface does not depend on the type of interface the factory is supporting.
 */
class ProxyObjectFactoryBase {
public:
    virtual ~ProxyObjectFactoryBase() = default;

    /*
     * Destroys all proxy objects created by this factory so far.
     */
    virtual void clearCache() = 0;

    /*
     * Returns the target object for the given proxy if the proxy object exists.
     * Otherwise returns CORBA::Object::_nil() reference
     */
    virtual CORBA::Object_ptr getRemoteObject(CORBA::Object_ptr obj) = 0;
    
    /*
     * Runs the garbage collector that removes proxy objects with dead targets.
     */
    virtual void runGarbageCollector() = 0;

   /*
     * Returns type id of the objects created by this factory.
     */
    virtual const std::string & getTypeId() = 0;

    /*
     * Extracts the object id from the \p ref object reference and checks the
     * local cache for a proxy object with the same id. If no proxy object is
     * found the function will create a new proxy object and set the original
     * reference as its target object for requests forwarding.
     * If a proxy object with the same id already exists, the function will
     * replace its target reference with \p ref if the \p ref is not a reference
     * to the proxy object itself.
     *
     * \return The local proxy object reference.
     */
    virtual CORBA::Object_var createProxyObject(CORBA::Object_ptr ref) = 0;

protected:
    static std::string getObjectIdFromIOR(const std::string & ior);
};

#endif /* IPCGATEWAYPROXY_PROXYOBJECTFACTORYBASE_HPP_ */
