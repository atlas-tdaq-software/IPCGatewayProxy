# IPC gateway proxy application

This package implements a binary which can be used for forwarding CORBA/IPC
requests for the ATLAS TDAQ services through a gateway machine connecting 
two isolated networks. 

## Getting Started

The package uses the ATLAS TDAQ software release and therefore should to be built using the
corresponding CMAKE environment. Here you can find detailed instruction on how to set it up:
https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltCMake


## The content of the package

* **IPCGatewayProxy** – contains header files for the gateway proxy implementation

* **server** – contains implementation of the gateway proxy

* **src** – contains implementation of the requests forwarding plugins for the CORBA-based TDAQ interfaces 
		

## Using the gateway proxy

The gateway proxy application has to be started on the gateway computer that 
is used for connecting two isolated networks.

### Starting the gateway proxy application

In order to start the gateway proxy application one has to use the following command:

```
ipc_gateway_proxy -i <private_IP> -I <public_IP> -p <private_port> -P <public_port>
```

This command will start the proxy application and instructs it to listen on the given TCP 
ports on both the public and the private network interfaces.

### Using the other TDAQ applications in the private domain

In the private network domain, the TDAQ_IPC_INIT_REF environment variable must be set 
to the value printed by the gateway proxy application, which has the following format:

```
corbaloc:iiop:private_IP:private_port/%ffipc/partition%00initial/ipc/partition/initial
```

:exclamation: **IMPORTANT NOTE:**
Using the same private port number and IP address for the gateway proxy application will 
result in obtaining the same value for the TDAQ_IPC_INIT_REF environment. Therefore by 
fixing the private port number one can have persistent reference that does not need to be 
changed when the gateway proxy application is restarted.

### Starting TDAQ applications in the public domain

No special action is required. The TDAQ applications in the public domain have to be started
in a usual way.
